	.file	"HelloWorld.c"
__SP_H__ = 0x3e
__SP_L__ = 0x3d
__SREG__ = 0x3f
__tmp_reg__ = 0
__zero_reg__ = 1
	.text
.Ltext0:
	.cfi_sections	.debug_frame
	.section	.text.HeartBeat,"ax",@progbits
.global	HeartBeat
	.type	HeartBeat, @function
HeartBeat:
.LFB6:
	.file 1 "Z:\\tmp\\HelloWorld\\Build\\HelloWorld.c"
	.loc 1 9 0
	.cfi_startproc
/* prologue: function */
/* frame size = 0 */
/* stack size = 0 */
.L__stack_usage = 0
	.loc 1 15 0
	lds r24,illuminosity.1999
	cpi r24,lo8(-1)
	brne .L2
	.loc 1 16 0
	sts control.2000,__zero_reg__
	rjmp .L3
.L2:
	.loc 1 17 0
	cpse r24,__zero_reg__
	rjmp .L4
	.loc 1 18 0
	ldi r25,lo8(1)
	sts control.2000,r25
	rjmp .L5
.L4:
	.loc 1 21 0
	lds r25,control.2000
	cpse r25,__zero_reg__
	rjmp .L5
.L3:
	.loc 1 22 0
	subi r24,lo8(-(-1))
	sts illuminosity.1999,r24
	rjmp .L6
.L5:
	.loc 1 24 0
	subi r24,lo8(-(1))
	sts illuminosity.1999,r24
.L6:
	.loc 1 27 0
	lds r24,illuminosity.1999
	sts 179,r24
	ret
	.cfi_endproc
.LFE6:
	.size	HeartBeat, .-HeartBeat
	.section	.text.main,"ax",@progbits
.global	main
	.type	main, @function
main:
.LFB7:
	.loc 1 30 0
	.cfi_startproc
/* prologue: function */
/* frame size = 0 */
/* stack size = 0 */
.L__stack_usage = 0
	.loc 1 31 0
	ldi r22,lo8(-112)
	ldi r23,lo8(-48)
	ldi r24,lo8(3)
	ldi r25,0
	call Usart_Init
.LVL0:
	.loc 1 34 0
	sbi 0x4,3
	.loc 1 35 0
	out 0x5,__zero_reg__
	.loc 1 36 0
	ldi r30,lo8(-80)
	ldi r31,0
	ldi r24,lo8(5)
	std Z+1,r24
	.loc 1 37 0
	ldi r24,lo8(-127)
	st Z,r24
	.loc 1 40 0
	ldi r18,lo8(gs(HeartBeat))
	ldi r19,hi8(gs(HeartBeat))
	ldi r20,lo8(-128)
	ldi r22,lo8(4)
	ldi r24,0
	call RegisterCompareMatchInterrupt
.LVL1:
	.loc 1 42 0
	call InitializeStateEventFramework
.LVL2:
	.loc 1 44 0
	ldi r24,0
	ldi r25,0
	ret
	.cfi_endproc
.LFE7:
	.size	main, .-main
	.section	.bss.control.2000,"aw",@nobits
	.type	control.2000, @object
	.size	control.2000, 1
control.2000:
	.zero	1
	.section	.bss.illuminosity.1999,"aw",@nobits
	.type	illuminosity.1999, @object
	.size	illuminosity.1999, 1
illuminosity.1999:
	.zero	1
	.text
.Letext0:
	.file 2 "C:\\tmp\\avr\\ToolChain\\AvrLib\\include/Atmega328P.h"
	.file 3 "c:\\tmp\\avr\\toolchain\\avr8-gnu-toolchain-win32_x86\\avr\\include\\stdint.h"
	.file 4 "C:\\tmp\\avr\\ToolChain\\AvrLib\\include/avrlib.h"
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.long	0x242
	.word	0x2
	.long	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.long	.LASF33
	.byte	0xc
	.long	.LASF34
	.long	.Ldebug_ranges0+0
	.long	0
	.long	0
	.long	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.long	.LASF0
	.uleb128 0x3
	.long	.LASF8
	.byte	0x3
	.byte	0x7e
	.long	0x37
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.long	.LASF1
	.uleb128 0x4
	.byte	0x2
	.byte	0x5
	.string	"int"
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.long	.LASF2
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.long	.LASF3
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.long	.LASF4
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.long	.LASF5
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.long	.LASF6
	.uleb128 0x5
	.long	.LASF10
	.byte	0x3
	.byte	0x2
	.byte	0xc6
	.long	0x9f
	.uleb128 0x6
	.string	"PIN"
	.byte	0x2
	.byte	0xc8
	.long	0x9f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x6
	.string	"DDR"
	.byte	0x2
	.byte	0xc9
	.long	0x9f
	.byte	0x2
	.byte	0x23
	.uleb128 0x1
	.uleb128 0x7
	.long	.LASF7
	.byte	0x2
	.byte	0xca
	.long	0x9f
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.byte	0
	.uleb128 0x8
	.long	0x2c
	.uleb128 0x3
	.long	.LASF9
	.byte	0x2
	.byte	0xcb
	.long	0x68
	.uleb128 0x9
	.long	.LASF11
	.byte	0x5
	.byte	0x2
	.word	0x114
	.long	0x108
	.uleb128 0xa
	.long	.LASF12
	.byte	0x2
	.word	0x116
	.long	0x9f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xa
	.long	.LASF13
	.byte	0x2
	.word	0x117
	.long	0x9f
	.byte	0x2
	.byte	0x23
	.uleb128 0x1
	.uleb128 0xa
	.long	.LASF14
	.byte	0x2
	.word	0x118
	.long	0x9f
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.uleb128 0xa
	.long	.LASF15
	.byte	0x2
	.word	0x119
	.long	0x9f
	.byte	0x2
	.byte	0x23
	.uleb128 0x3
	.uleb128 0xa
	.long	.LASF16
	.byte	0x2
	.word	0x11a
	.long	0x9f
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.byte	0
	.uleb128 0xb
	.long	.LASF17
	.byte	0x2
	.word	0x11b
	.long	0xaf
	.uleb128 0xc
	.byte	0x1
	.long	0x37
	.byte	0x4
	.byte	0x3e
	.long	0x139
	.uleb128 0xd
	.long	.LASF18
	.byte	0
	.uleb128 0xd
	.long	.LASF19
	.byte	0x1
	.uleb128 0xd
	.long	.LASF20
	.byte	0x2
	.uleb128 0xd
	.long	.LASF21
	.byte	0x3
	.byte	0
	.uleb128 0xc
	.byte	0x1
	.long	0x37
	.byte	0x4
	.byte	0x4a
	.long	0x164
	.uleb128 0xd
	.long	.LASF22
	.byte	0
	.uleb128 0xd
	.long	.LASF23
	.byte	0x1
	.uleb128 0xd
	.long	.LASF24
	.byte	0x2
	.uleb128 0xd
	.long	.LASF25
	.byte	0x3
	.uleb128 0xd
	.long	.LASF26
	.byte	0x4
	.byte	0
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.long	.LASF27
	.uleb128 0xe
	.byte	0x1
	.long	.LASF35
	.byte	0x1
	.byte	0x9
	.long	.LFB6
	.long	.LFE6
	.byte	0x3
	.byte	0x92
	.uleb128 0x20
	.sleb128 2
	.byte	0x1
	.long	0x1a7
	.uleb128 0xf
	.long	.LASF28
	.byte	0x1
	.byte	0xb
	.long	0x2c
	.byte	0x5
	.byte	0x3
	.long	illuminosity.1999
	.uleb128 0xf
	.long	.LASF29
	.byte	0x1
	.byte	0xc
	.long	0x2c
	.byte	0x5
	.byte	0x3
	.long	control.2000
	.byte	0
	.uleb128 0x10
	.byte	0x1
	.long	.LASF36
	.byte	0x1
	.byte	0x1e
	.byte	0x1
	.long	0x3e
	.long	.LFB7
	.long	.LFE7
	.byte	0x3
	.byte	0x92
	.uleb128 0x20
	.sleb128 2
	.byte	0x1
	.long	0x21d
	.uleb128 0x11
	.long	.LVL0
	.long	0x21d
	.long	0x1e7
	.uleb128 0x12
	.byte	0xc
	.byte	0x66
	.byte	0x93
	.uleb128 0x1
	.byte	0x67
	.byte	0x93
	.uleb128 0x1
	.byte	0x68
	.byte	0x93
	.uleb128 0x1
	.byte	0x69
	.byte	0x93
	.uleb128 0x1
	.byte	0x5
	.byte	0xc
	.long	0x3d090
	.byte	0
	.uleb128 0x11
	.long	.LVL1
	.long	0x22b
	.long	0x213
	.uleb128 0x12
	.byte	0x1
	.byte	0x68
	.byte	0x1
	.byte	0x30
	.uleb128 0x12
	.byte	0x1
	.byte	0x66
	.byte	0x1
	.byte	0x34
	.uleb128 0x12
	.byte	0x1
	.byte	0x64
	.byte	0x2
	.byte	0x9
	.byte	0x80
	.uleb128 0x12
	.byte	0x6
	.byte	0x62
	.byte	0x93
	.uleb128 0x1
	.byte	0x63
	.byte	0x93
	.uleb128 0x1
	.byte	0x5
	.byte	0x3
	.long	HeartBeat
	.byte	0
	.uleb128 0x13
	.long	.LVL2
	.long	0x238
	.byte	0
	.uleb128 0x14
	.byte	0x1
	.byte	0x1
	.long	.LASF30
	.long	.LASF30
	.byte	0x4
	.word	0x135
	.uleb128 0x15
	.byte	0x1
	.byte	0x1
	.long	.LASF31
	.long	.LASF31
	.byte	0x4
	.byte	0xf9
	.uleb128 0x15
	.byte	0x1
	.byte	0x1
	.long	.LASF32
	.long	.LASF32
	.byte	0x4
	.byte	0xbd
	.byte	0
	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x4
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.uleb128 0x2117
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.uleb128 0x2117
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x410a
	.byte	0
	.uleb128 0x2
	.uleb128 0xa
	.uleb128 0x2111
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x4109
	.byte	0
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.uleb128 0x2007
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.uleb128 0x2007
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_aranges,"",@progbits
	.long	0x24
	.word	0x2
	.long	.Ldebug_info0
	.byte	0x4
	.byte	0
	.word	0
	.word	0
	.long	.LFB6
	.long	.LFE6-.LFB6
	.long	.LFB7
	.long	.LFE7-.LFB7
	.long	0
	.long	0
	.section	.debug_ranges,"",@progbits
.Ldebug_ranges0:
	.long	.LFB6
	.long	.LFE6
	.long	.LFB7
	.long	.LFE7
	.long	0
	.long	0
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.section	.debug_str,"MS",@progbits,1
.LASF30:
	.string	"Usart_Init"
.LASF28:
	.string	"illuminosity"
.LASF12:
	.string	"TCCRA"
.LASF26:
	.string	"TimerFrequency_Div1024"
.LASF15:
	.string	"OCRA"
.LASF16:
	.string	"OCRB"
.LASF11:
	.string	"TCNT8_T_tag"
.LASF1:
	.string	"unsigned char"
.LASF14:
	.string	"TCNT"
.LASF33:
	.string	"GNU C99 5.4.0 -mn-flash=1 -mno-skip-bug -mmcu=avr5 -g2 -O1 -std=gnu99 -funsigned-char -funsigned-bitfields -ffunction-sections -fdata-sections -fpack-struct -fshort-enums"
.LASF4:
	.string	"long unsigned int"
.LASF29:
	.string	"control"
.LASF32:
	.string	"InitializeStateEventFramework"
.LASF20:
	.string	"CompareMatchSource3"
.LASF36:
	.string	"main"
.LASF2:
	.string	"unsigned int"
.LASF22:
	.string	"TimerFrequency_Div1"
.LASF6:
	.string	"long long unsigned int"
.LASF13:
	.string	"TCCRB"
.LASF23:
	.string	"TimerFrequency_Div8"
.LASF35:
	.string	"HeartBeat"
.LASF27:
	.string	"sizetype"
.LASF8:
	.string	"uint8_t"
.LASF5:
	.string	"long long int"
.LASF24:
	.string	"TimerFrequency_Div64"
.LASF7:
	.string	"PORT"
.LASF10:
	.string	"GPIO_T_tag"
.LASF18:
	.string	"CompareMatchSource1"
.LASF19:
	.string	"CompareMatchSource2"
.LASF21:
	.string	"CompareMatchSource4"
.LASF9:
	.string	"GPIO_T"
.LASF34:
	.string	"Z:\\tmp\\HelloWorld\\Build\\HelloWorld.c"
.LASF31:
	.string	"RegisterCompareMatchInterrupt"
.LASF25:
	.string	"TimerFrequency_Div256"
.LASF3:
	.string	"long int"
.LASF0:
	.string	"signed char"
.LASF17:
	.string	"TCNT8_T"
	.ident	"GCC: (AVR_8_bit_GNU_Toolchain_3.6.2_1759) 5.4.0"
.global __do_clear_bss

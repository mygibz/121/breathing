#include <Atmega328P.h>
#include <avrlib.h>
#include <RegisterAccess.h>
#define F_CPU 16000000
#include <util/delay.h>

#define Port_5_mask 0x20;

void HeartBeat() {
    // both variables will stay around the next time the function is called
    static uint8_t illuminosity = 0;
    static uint8_t control = 0;
	
    // checks if the direction needs to be changesd
   if (illuminosity >= 255)
        control = 0;
    else if (illuminosity <= 0)
        control = 1;

    // increases or decreases the brightness, depending on that value
    if (control == 0) 
        illuminosity--;
    else 
        illuminosity++;

    // apply value
    Tcnt2.OCRA = illuminosity;
}

int main(void) {
	Usart_Init(250000);

    // set configs
    PortB.DDR |= 0x8;
    PortB.PORT = 0;
    Tcnt2.TCCRB = 0x5;
    Tcnt2.TCCRA = 0x81;

    // start timer
    RegisterCompareMatchInterrupt(CompareMatchSource1, TimerFrequency_Div1024, 0x80, HeartBeat);

    InitializeStateEventFramework();
    return 0;
}



